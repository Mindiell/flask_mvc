# Flask MVC

Simple skeleton for using a website with Flask micro-framework through MVC pattern.

License: AGPLv3+


## Usage

You can clone this repository:

    git clone --depth 1 --single-branch https://framagit.org/Mindiell/flask_mvc.git

And then you can copy useful content for your project and start it:

    cp -r git_repo_of_project/project/* your_project/
    cd your_project
    python3 -m venv venv
    source venv/bin/activate
    pip install -r requirements-dev.txt


Finally, you can run your application using:

    flask --app server --debug run


## Project structure

Structure is simple. Names are taken from Django in order to help
comprehension/transition:

* app                   application folder
    * views             folder for controllers
    * models            folder for models
    * templates         folder for templates
    settings.py         contains some configuration
    urls.py             contains all urls of your application
* commands              folder containing custom CLI Flask commands based on click
* locale                folder containing translations
* static                folder for static contents
* tests                 folder for testing
* server.py             used to launch web application


## Configuration

All parameters used by flask_mvc are present in the _settings.py_ file. You should adapt
each value using environment variables. You can set your environment accordingly or use
a dotenv file _.env_.

### FLASK_DEBUG parameter

Obviously, this parameter should not be set in production. Its default value is False.

### SECRET_KEY parameter

This parameter **MUST** be changed before using your application in production.

### BCRYPT_ROUNDS parameter

Set to 6 by default, this value is too low for production. You **MUST** change it too
(15 should be a good value).

### SQLALCHEMY_DATABASE_URI parameter

This parameter should be changed too as it maybe not meets your requirements.

### Babel configuration

You can set the default locale to use and a list of languages you would like to be able
to translate to.


## Core included

User management is set on default project. This means you got URLs for register, login,
and logout.

User model is basic but can be modified as needed.


## Translations

use Flask-Babel for translations

Extraction of strings to translate:

    pybabel extract -F babel.cfg -o messages.pot ./

Initialisation of a specific language:

    pybabel init -i messages.pot -d translations -l fr

Update of strings for all translated languages:

    pybabel update -i messages.pot -d translations

Compile each languages to use them:

    pybabel compile -d translations
