import os

BASE_DIR = os.path.abspath(os.path.dirname(__file__))

# Security configuration
DEBUG = os.environ.get("FLASK_DEBUG", False)
SECRET_KEY = os.environ.get("SECRET_KEY", "Choose a secret key")

# JINJA configuration
JINJA_ENV = {
    "TRIM_BLOCKS": True,
    "LSTRIP_BLOCKS": True,
}

# Database configuration
SQLALCHEMY_DATABASE_URI = os.environ.get(
    "SQLALCHEMY_DATABASE_URI",
    "sqlite:///" + os.path.join(BASE_DIR, "db.sqlite3"),
)
SQLALCHEMY_TRACK_MODIFICATIONS = False

# Bcrypt configuration
BCRYPT_ROUNDS = os.environ.get("BCRYPT_ROUNDS", 6)

# Babel configuration
BABEL_DEFAULT_LOCALE = os.environ.get("BABEL_DEFAULT_LOCALE", "en")
AVAILABLE_LANGUAGES = {
    "en": "English",
}
