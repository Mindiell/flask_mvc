import pytest

from server import application


@pytest.fixture
def client():
    return application.test_client()
