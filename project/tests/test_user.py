from tests.utils import client


class TestUser:
    def test_anonymous_admin(self, client):
        result = client.get("/admin/")
        assert "Login" in str(result.data)
