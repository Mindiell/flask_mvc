from flask_admin import Admin, expose, AdminIndexView
from flask_babel import Babel
from flask_login import LoginManager
from flask_migrate import Migrate
from flask_restful import Api
from flask_sqlalchemy import SQLAlchemy

from app.forms.user import UserLoginForm


class IndexView(AdminIndexView):
    @expose("/")
    def index(self):
        form = UserLoginForm()
        return self.render("admin/index.html", form=form)


admin = Admin(index_view=IndexView())
api = Api()
babel = Babel()
db = SQLAlchemy()
login_manager = LoginManager()
migrate = Migrate()
