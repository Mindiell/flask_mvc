from app.views import admin, core, user

routes = [
    # Admin routes
    ("/admin/login", admin.Admin.as_view("login"), ["GET", "POST"]),
    # User routes
    ("/user/register", user.User.as_view("register"), ["GET", "POST"]),
    ("/user/login", user.User.as_view("login"), ["GET", "POST"]),
    ("/user/logout", user.User.as_view("logout")),
    # Core routes
    ("/", core.Core.as_view("index")),
]

apis = [
    # Add your own URLs for API
]
