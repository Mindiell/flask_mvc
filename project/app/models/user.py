from argon2 import PasswordHasher
from flask import current_app

from app import admin, db
from app.models import Model, AdminView


def get_user(user_id):
    return UserModel.query.get(user_id)


class UserModel(db.Model, Model):
    __tablename__ = "user"
    id: db.Mapped[int] = db.mapped_column(primary_key=True)
    login: db.Mapped[str] = db.mapped_column(db.String(100), unique=True)
    password_hash: db.Mapped[str] = db.mapped_column(db.String(97))
    email: db.Mapped[str] = db.mapped_column(db.String(200), nullable=True)
    active: db.Mapped[bool] = db.mapped_column(default=False)
    admin: db.Mapped[bool] = db.mapped_column(default=False)

    @property
    def password(self):
        return self.password_hash

    def get_password_hasher(self):
        return PasswordHasher(
            time_cost=current_app.config["ARGON2_TIME_COST"],
            memory_cost=current_app.config["ARGON2_MEMORY_COST"],
            parallelism=current_app.config["ARGON2_PARALLELISM"],
            hash_len=current_app.config["ARGON2_HASH_LEN"],
            salt_len=current_app.config["ARGON2_SALT_LEN"],
            encoding=current_app.config["ARGON2_ENCODING"],
        )

    @password.setter
    def password(self, password):
        self.password_hash = self.get_password_hasher().hash(password)

    def check_password(self, password):
        try:
            return self.get_password_hasher().verify(self.password_hash, password)
        except:
            return False

    @property
    def is_active(self):
        return self.active or False

    @property
    def is_anonymous(self):
        return self.id is None

    @property
    def is_authenticated(self):
        return self.id is not None

    def get_id(self):
        return str(self.id)


class AdminUserView(AdminView):
    column_default_sort = "login"
    column_exclude_list = ["password_hash"]

    def on_model_change(self, form, model, is_created):
        if len(form.password_hash.data) < 97:
            model.password = form.password_hash.data


admin.add_view(AdminUserView(UserModel, db.session, name="User"))
