from datetime import datetime

from flask import current_app, redirect, request, url_for
from flask_admin.contrib.sqla import ModelView
from flask_login import current_user

from app import db


class AdminView(ModelView):
    def is_accessible(self):
        if not current_user.is_authenticated:
            return False
        return current_user.is_authenticated and current_user.admin

    def inaccessible_callback(self, name, **kwargs):
        return redirect(url_for("admin.login", next=request.url))


class Model:
    def save(self):
        is_existing = True
        if len(self.__table__.primary_key.columns.keys()) > 1:
            is_existing = False
        else:
            for column_name in self.__table__.primary_key.columns.keys():
                column = self.__getattribute__(column_name)
                if column is None or column == "":
                    is_existing = False
        if not is_existing:
            db.session.add(self)
        if not current_app.config["TESTING"]:
            db.session.commit()

    def delete(self):
        is_existing = True
        for column_name in self.__table__.primary_key.columns.keys():
            column = self.__getattribute__(column_name)
            if column is None or column == "":
                is_existing = False
        if is_existing:
            db.session.delete(self)
            if not current_app.config["TESTING"]:
                db.session.commit()

    def serialize(self):
        result_dict = {}
        for key in self.__table__.columns.keys():
            if not key.startswith("_"):
                if isinstance(getattr(self, key), datetime):
                    result_dict[key] = getattr(self, key).strftime("%Y-%m-%d")
                else:
                    result_dict[key] = getattr(self, key)
        return result_dict
