from flask import g, redirect, url_for, flash, render_template, session
from flask_login import login_user, logout_user

from app.forms.user import UserLoginForm, UserRegisterForm
from app.models.user import UserModel
from app.views import BaseView


class User(BaseView):
    def register(self):
        g.form = UserRegisterForm()
        if g.form.validate_on_submit():
            user = UserModel.query.filter_by(login=g.form.login.data).first()
            if not user:
                if g.form.password.data != g.form.password_second.data:
                    flash("Passwords are not identicals.", "warning")
                else:
                    user = UserModel(
                        login=g.form.login.data,
                        password=g.form.password.data,
                    )
                    user.save()
                    flash("User created!", "success")
                    return redirect(url_for("core.index"))
            else:
                flash("User already exists, please choose another login.", "warning")
        for field, message in g.form.errors.items():
            flash("%s: %s" % (field, message[0]))
        return render_template("user/register.html")

    def login(self):
        g.form = UserLoginForm()
        if g.form.validate_on_submit():
            user = UserModel.query.filter_by(login=g.form.login.data).first()
            if user and user.check_password(g.form.password.data):
                if user.is_active:
                    login_user(user)
                    return redirect(url_for("core.index"))
                else:
                    flash("Account not active.", "warning")
            else:
                flash("Bad login or password.", "warning")
        for field, message in g.form.errors.items():
            flash("%s: %s" % (field, message[0]))
        return render_template("user/login.html")

    def logout(self):
        logout_user()
        session.clear()
        return redirect(url_for("core.index"))
