from flask import g, redirect, url_for, flash, session
from flask_login import login_user, logout_user
import flask_admin

from app import admin
from app.forms.user import UserLoginForm
from app.models.user import UserModel
from app.views import BaseView


class Admin(BaseView):
    def login(self):
        g.form = UserLoginForm()
        if g.form.validate_on_submit():
            user = UserModel.query.filter_by(login=g.form.login.data).first()
            if user is not None and user.check_password(g.form.password.data):
                if user.is_active:
                    login_user(user)
                    return redirect(url_for("admin.index"))
                else:
                    flash("Account not active.", "warning")
            else:
                flash("Bad login or password.", "warning")
        for field, message in g.form.errors.items():
            flash("%s: %s" % (field, message[0]))
        return redirect(url_for("admin.index"))


class LogoutView(flask_admin.BaseView):
    @flask_admin.expose("/")
    def index(self):
        logout_user()
        session.clear()
        return redirect(url_for("admin.index"))


# TODO: Display this link in Admin menu only if user is authenticated
admin.add_view(LogoutView(name="Logout", endpoint="logout"))
