from flask import render_template

from app.views import BaseView


class Core(BaseView):
    def index(self):
        return render_template("core/index.html")
