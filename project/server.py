import flask

from app import admin, api, babel, db, login_manager, migrate
from app.models.user import get_user
from app.routes import apis, routes
from commands import commands


application = flask.Flask(__name__, template_folder="app/templates")
application.config.from_object("settings")

if "JINJA_ENV" in application.config:
    jinja_env = application.config["JINJA_ENV"]
    application.jinja_env.trim_blocks = jinja_env["TRIM_BLOCKS"]
    application.jinja_env.lstrip_blocks = jinja_env["LSTRIP_BLOCKS"]

# Loading routes
for route in routes:
    if len(route) < 3:
        application.add_url_rule(route[0], route[1].__name__, route[1], methods=["GET"])
    else:
        application.add_url_rule(
            route[0], route[1].__name__, route[1], methods=route[2]
        )
# Loading API routes
for route in apis:
    api.add_resource(route[1], route[0])


# Manage locale
def get_locale():
    return flask.session.get("locale", flask.current_app.config["BABEL_DEFAULT_LOCALE"])


# Manage user
@login_manager.user_loader
def load_user(user_id):
    return get_user(user_id)


# Initialisation of extensions
admin.init_app(application)
api.init_app(application)
babel.init_app(application, locale_selector=get_locale)
db.init_app(application)
login_manager.init_app(application)
migrate.init_app(application, db)

# Manage commands
for command in commands:
    application.cli.add_command(command)
